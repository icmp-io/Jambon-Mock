#/bin/bash

##################################################################
# STARTS JAMBON MOCK API
#
# Usage:
#   bash start.sh
#
# Role:
#   Starts Jambon mock API from associated Docker container
#   perorms the following tasks:
#     - Change default Jre from Java 7 -> Java 8
#     - run maven task clean and package
#     - run server (Jambon mock) on http://localhost:4567
#
#
#
# Version:
#   0.1
# Author:
#   Yann Feunteun
#   <yfe@protonmail.com>
#

# Change default Jre from Java 7 -> Java 8
update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java || echo "Error, failed to change default JRE" > /dev/stderr

# Build App with maven
mvn clean && mvn package -DskipTests

# Run server on http://127.0.0.1:4567
java -jar /local/work/target/App-0.0.1-SNAPSHOT-jar-with-dependencies.jar
