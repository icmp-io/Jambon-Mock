#
# Jambon Mock Dockerfile
#
#
# pull base image.
FROM java:8

# maintainer details
MAINTAINER Yann Feunteun "yfe@protonmail.com"

# update packages and install maven
  RUN  \
  export DEBIAN_FRONTEND=noninteractive && \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y vim wget curl git maven

# create working directory
RUN mkdir -p /local/work
WORKDIR /local/work

# expose server port
EXPOSE 4567

# Add current directory to wordir
ADD . .

# run server
CMD ["bash", "/local/work/start.sh"]

