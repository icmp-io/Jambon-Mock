package io.icmp.jambonmock.App;

import com.google.gson.Gson;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import spark.Spark;
import spark.utils.IOUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class UserControllerIntegrationTest {

	@BeforeClass
	public static void beforeClass() {
		App.main(null);
	}

	@AfterClass
	public static void afterClass() {
		Spark.stop();
	}

	@Test
	public void aNewProbeShouldBeCreated() {
		TestResponse res = request("POST", "/probes?ip_version=4&type=icmp_echo&target=spacex.com&interval_s=30&timeout_s=1");
		Map<String, String> json = res.json();
		assertEquals(200, res.status);
		assertEquals("4", json.get("ip_version"));
		assertEquals("icmp_echo", json.get("type"));
		assertEquals("spacex.com", json.get("target"));
		assertEquals("30", json.get("interval_s"));
		assertEquals("1", json.get("timeout_s"));
		assertNotNull(json.get("id"));
	}

	private TestResponse request(String method, String path) {
		try {
			URL url = new URL("http://localhost:4567" + path);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod(method);
			connection.setDoOutput(true);
			connection.connect();
			String body = IOUtils.toString(connection.getInputStream());
			return new TestResponse(connection.getResponseCode(), body);
		} catch (IOException e) {
			e.printStackTrace();
			fail("Sending request failed: " + e.getMessage());
			return null;
		}
	}

	private static class TestResponse {

		public final String body;
		public final int status;

		public TestResponse(int status, String body) {
			this.status = status;
			this.body = body;
		}

		public Map<String,String> json() {
			return new Gson().fromJson(body, HashMap.class);
		}
	}
}