package io.icmp.jambonmock.App;

/**
 * Hello world!
 *
 */
public class App 
{
	public static void main( String[] args )
	{		
		ProbeService probeService = new ProbeService();
		probeService.createProbe("4", "icmp_echo", "google.com", "30", "1");
		probeService.createProbe("4", "icmp_echo", "netflix.com", "30", "1");

		new ProbeController(probeService);
	}
}

