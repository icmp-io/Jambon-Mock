package io.icmp.jambonmock.App;

import static spark.Spark.*;
import spark.Request;
import spark.Response;
import spark.Route;

public class ProbeController {

	public ProbeController(final ProbeService probeService) {

		get("/probes", new Route() {
			public Object handle(Request request, Response response) {
				// process request
				return probeService.getAllProbes();
			}
		}, JsonUtil.json());

		after((req, res) -> {

			res.type("application/json");

		});

		exception(IllegalArgumentException.class, (e, req, res) -> {
			res.status(400);
			res.body(JsonUtil.toJson((new ResponseError(e))));
		});

		get("/probes/:id", (req, res) -> {
			String id = req.params(":id");
			Probe probe = probeService.getProbe(id);
			if (probe != null) {
				return probe;
			}
			res.status(400);
			return new ResponseError("No probe with id " + id + " found");
		}, JsonUtil.json());
		// more routes


		post("/probes", (req, res) -> probeService.createProbe(
				req.queryParams("ip_version"),
				req.queryParams("type"),
				req.queryParams("target"),
				req.queryParams("interval_s"),
				req.queryParams("timeout_s")
				), JsonUtil.json());


		put("/probes/:id", (req, res) -> probeService.updateProbe(
				req.params(":id"),
				req.queryParams("ip_version"),
				req.queryParams("type"),
				req.queryParams("target"),
				req.queryParams("interval_s"),
				req.queryParams("timeout_s")
				), JsonUtil.json());


	}

}