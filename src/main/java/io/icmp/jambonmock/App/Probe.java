package io.icmp.jambonmock.App;

import java.util.LinkedHashMap;
import java.util.Random;

public class Probe {

	private static Integer counter = 0;
	
	private String id;
	private String ip_version;
	private String type;
	private String target;
	private String interval_s;
	private String timeout_s; 
	private LinkedHashMap<Integer, Integer> measures;

	
	// getter/setter

	public LinkedHashMap<Integer, Integer> getMeasures() {
		return measures;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIp_version() {
		return ip_version;
	}
	public void setIp_version(String ip_version) {
		this.ip_version = ip_version;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getInterval_s() {
		return interval_s;
	}
	public void setInterval_s(String interval_s) {
		this.interval_s = interval_s;
	}
	public String getTimeout_s() {
		return timeout_s;
	}
	public void setTimeout_s(String timeout_s) {
		this.timeout_s = timeout_s;
	}
	
	public Probe(String ip_version, String type, String target, String interval_s, String timeout_s)
	{
		this.id = counter.toString();
		counter ++;
		this.ip_version = ip_version;
		this.type = type;
		this.target = target;
		this.interval_s = interval_s;
		this.timeout_s = timeout_s;
		
		// Generates 100 measures (in ms) spaced by interval_s
		// always under timeout_s
		Integer timeout_as_string = Integer.parseInt(timeout_s);
		Integer interval_as_string = Integer.parseInt(interval_s);
		Random random = new Random();
		this.measures = new LinkedHashMap<Integer, Integer>();
		for (int i = 0; i < 100; ++i) {
			int latency = random.nextInt(timeout_as_string*1000 - 0) + 0;
			this.measures.put(i*interval_as_string, latency);
		}
	}

}