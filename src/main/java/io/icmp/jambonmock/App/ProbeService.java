package io.icmp.jambonmock.App;

import java.util.LinkedList;
import java.util.List;

public class ProbeService {

	private List<Probe> allprobes = new LinkedList<Probe>();

	// returns a list of all probes

	public List<Probe> getAllProbes() {
		return allprobes;  
	}

	// returns a single probe by id

	public Probe getProbe(String id) {
		try {
			Integer id_as_integer = Integer.parseInt(id);
			if (id_as_integer < 0 || id_as_integer > allprobes.size()) {
				return null;
			}
			return allprobes.get(id_as_integer);
		} catch (Exception e) {
			return null;
		}

	}

	// creates a new probe

	public Probe createProbe(String ip_version, String type, String target, String interval_s, String timeout_s) {
		Probe newProbe = new Probe(ip_version, type, target, interval_s, timeout_s);
		allprobes.add(newProbe);
		return newProbe;
	}

	// updates an existing probe

	public Probe updateProbe(String id, String ip_version, String type, String target, String interval_s, String timeout_s) {
		return null;  }

}